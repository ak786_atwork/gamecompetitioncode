import java.util.*;

public class ParenthesisChecker {
    private static final char OPENING_SMALL_BRACE = '(';
    private static final  char CLOSING_SMALL_BRACE = ')';
    private static final  char OPENING_SQUARE_BRACE = '[';
    private static  final char CLOSING_SQUARE_BRACE = ']';
    private static  final char OPENING_CURLY_BRACE = '{';
    private static final  char CLOSING_CURLY_BRACE = '}';

    public static void main (String arg[]) {

        Scanner sc = new Scanner(System.in);
        Stack<Character> characters = new Stack<Character>();

        int testCases = sc.nextInt();

        boolean found = false;

        int i = 1;

        while (testCases > 0) {

            String exp = sc.nextLine();
            i = 1;
            characters.clear();
            characters.push(exp.charAt(0));
            while (i < exp.length()) {
                found = false;
                switch( exp.charAt(i)) {
                    case CLOSING_SQUARE_BRACE :
                        //
                        if(OPENING_SQUARE_BRACE == characters.peek()) {
                            characters.pop();
                            found = true;
                        }
                        break;
                    case CLOSING_CURLY_BRACE :
                        //
                        if(OPENING_CURLY_BRACE == characters.peek()) {
                            characters.pop();
                            found = true;
                        }
                        break;
                    case CLOSING_SMALL_BRACE :
                        //
                        if(OPENING_SMALL_BRACE == characters.peek()) {
                            characters.pop();
                            found = true;
                        }
                        break;


                }
                if (!found) {
                    characters.push(exp.charAt(i));
                    found = false;
                }
                i++;
            }
            if (characters.size() == 0 ) {
                System.out.println("balanced");
            } else {
                System.out.println("not balanced");
            }
            testCases--;
        }

    }
}
