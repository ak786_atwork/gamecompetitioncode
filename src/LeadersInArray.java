import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

//working fine
public class LeadersInArray {

    public static void main(String arg[]) throws IOException {

//        Scanner sc = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

//        int testCases = 1;//Integer.parseInt(br.readLine());//sc.nextInt();
        int testCases = Integer.parseInt(br.readLine());//sc.nextInt();
        int size;
        int[] array;
        while (testCases > 0) {
//            size = sc.nextInt();
            size = Integer.parseInt(br.readLine());
            array = new int[size];
            array = handleArrayWithBufferedReader(br,array);
            System.out.println(findLeaders(array));
            testCases--;

        }
    }

    private static String findLeaders(int[] array) {
        StringBuffer leaders = new StringBuffer();
        ArrayList<String> list = new ArrayList<>();
//        String leaders = "";
        int maxNumberInRight = array[array.length -1];
        for (int i = array.length - 1; i > -1; i--) {
            if (array[i] >= maxNumberInRight) {
//                leaders = array[i] +" " + leaders;
//                leaders.reverse().append(0," "+array[i]);
                list.add(0, array[i] +" ");
                maxNumberInRight = array[i];
            }
        }
        list.forEach((e) -> leaders.append(e));
        return  leaders.toString(); //.reverse().toString();
    }

    private static int[] handleArrayWithBufferedReader(BufferedReader br, int[] array) throws IOException {
        // to read multiple integers line
        String line = br.readLine();
        String[] strs = line.trim().split("\\s+");

        // array elements input
        for (int i = 0; i < array.length; i++)
            array[i] = Integer.parseInt(strs[i]);

//        int array1[] = {1, 3 ,5, 2, 2};
        return array;
    }


}
