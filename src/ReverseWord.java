import java.util.*;

public class ReverseWord {

    public static void main (String arg[]) {

        Scanner sc = new Scanner(System.in);

        int testCases = sc.nextInt();
        String temp = "";
        sc.nextLine();
        while (testCases > 0) {
            temp = sc.nextLine();
            System.out.println(getReverseWord(temp));
            testCases--;

        }

    }

    public static String getReverseWord(String line)
    {
        StringBuilder stringBuilder = new StringBuilder();
        String temp = "";
        if (line.length() < 2)
            return line;
        int i = 0 ;
        while ( i < line.length() ) {
            if (line.charAt(i) != '.') {
                temp = "";
                while (i < line.length() && line.charAt(i) != '.') {
                    temp += line.charAt(i);
                    i++;
                }

                temp += ".";
                stringBuilder.insert(0, temp);
            } else {
                i++;
            }

        }
        //removing extra .
        return stringBuilder.substring(0,stringBuilder.length() - 1); // .toString();
    }
}
