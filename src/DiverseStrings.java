import javafx.scene.transform.Scale;

import java.util.Scanner;

public class DiverseStrings {

    public static  void main(String ar []) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String word;
        int[] alphabetCounter = new int[26];
        boolean characterEncounteredMultipleTimes = false;
        int position = 0;
        int wordsInSequence = 0;
        int minPosition = 1000;

        scanner.nextLine();
        while (n > 0) {
            word = scanner.nextLine();
            for (int i = 0;i < word.length();i++) {
                position = word.charAt(i) - 97;
                if (alphabetCounter[position] == 1) {
                    characterEncounteredMultipleTimes = true;
                    break;
                } else {
                    //position 0 , means first time
                    alphabetCounter[position]++;
                }
                minPosition = minPosition > position ? position : minPosition;
            }
            if (characterEncounteredMultipleTimes) {
                System.out.println("No");
            } else {
                while (minPosition < 26) {
                    if (alphabetCounter[minPosition] != 1)
                        break;
                    wordsInSequence++;
                    minPosition++;
                }
                if (wordsInSequence == word.length())
                    System.out.println("Yes");
                else
                    System.out.println("No");
            }
           n--;
            alphabetCounter = resetArray(alphabetCounter);
            wordsInSequence = 0;
            minPosition = 1000;
            characterEncounteredMultipleTimes =false;
        }
    }

    public static int[] resetArray(int array[]) {
        for (int i =0 ; i < 26; i++) {
            array[i] = 0;
        }
        return array;
    }
}
