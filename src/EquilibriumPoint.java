import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

//working fine

public class EquilibriumPoint {
    public static void main(String arg[]) throws IOException {

//        Scanner sc = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

//        int testCases = 1;//Integer.parseInt(br.readLine());//sc.nextInt();
        int testCases = Integer.parseInt(br.readLine());//sc.nextInt();
        int size;
        int[] array;
        while (testCases > 0) {
//            size = sc.nextInt();
            size = Integer.parseInt(br.readLine());
            array = new int[size];
            array = handleArrayWithBufferedReader(br,array);
            System.out.println(findEquilibriumPoint(array));
            testCases--;

        }
    }

    private static int[] handleArrayWithBufferedReader(BufferedReader br, int[] array) throws IOException {
        // to read multiple integers line
        String line = br.readLine();
        String[] strs = line.trim().split("\\s+");

//        Scanner sc = new Scanner(System.in);
        // array elements input
        for (int i = 0; i < array.length; i++)
            array[i] = Integer.parseInt(strs[i]);

//        int array1[] = {1, 3 ,5, 2, 2};
        return array;
    }

    private static int findEquilibriumPoint(int[] array) {
        int point = -1;
        int leftIndex = 0;
        int rightIndex = array.length - 1;
        int leftSum = 0;
        int rightSum = 0;

        if (array.length == 2)
            return -1;

        while ( leftIndex < rightIndex) {


            if (leftSum > rightSum) {
                rightSum += array[rightIndex];
                --rightIndex;
            } else if(leftSum < rightSum) {
                leftSum += array[leftIndex];
                ++leftIndex;
            } else {
                leftSum += array[leftIndex];
                rightSum += array[rightIndex];
                ++leftIndex;
                --rightIndex;
            }
        }
       return leftSum == rightSum ? leftIndex + 1 : -1;
    }

}
