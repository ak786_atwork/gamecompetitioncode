import java.util.Scanner;

// working fine

public class LongestStringPalindrome {


    public static void main(String arg[]) {

        Scanner sc = new Scanner(System.in);

        int testCases = sc.nextInt();
        String temp = "";
        String result = "";
        sc.nextLine();
        while (testCases > 0) {
            temp = sc.nextLine();
            result = find1(0, temp.length() - 1, 0, temp.length() - 1, 0, temp.toCharArray());
         /*   if (result.length() == 1)
                System.out.println(temp.charAt(0));
            else
                System.out.println(result);*/
            System.out.println(result);
            testCases--;

        }

    }

/*    // this approach is failing at some cases
    public static String find(int startIndex, int endIndex, int currentStartIndex, int currentEndIndex, int count, char[] word) {
        String result = "";
        String temp1, temp2;
        if (currentStartIndex < currentEndIndex) {
            //proceed further

            if (word[currentStartIndex] == word[currentEndIndex]) {
                //check other number
                result = find(startIndex, endIndex, ++currentStartIndex, --currentEndIndex, ++count, word);
            } else *//*if (word[currentStartIndex] != word[currentEndIndex] && count != 0)*//* {
//                //reset endpointer
//                startIndex = currentStartIndex;
//                //shifting backward
//                currentEndIndex = endIndex;
                count = 0;
                // left side same and pushing other to end
                if (currentStartIndex == startIndex) {
                    startIndex = ++currentStartIndex;
                    temp1 = find(startIndex, endIndex, currentStartIndex, endIndex, count, word);
                } else
                    temp1 = find(currentStartIndex, endIndex, currentStartIndex, endIndex, count, word);
                // pushing start to startindex and decrementing end side


                if (endIndex == currentEndIndex) {
                    endIndex = --currentEndIndex;
                    temp2 = find(startIndex, endIndex, startIndex, currentEndIndex, count, word);
                } else
                    temp2 = find(startIndex, currentEndIndex, startIndex, currentEndIndex, count, word);

                result = temp1.length() > temp2.length() ? temp1 : temp2;
            }
            *//*else {
                //not equal and count = 0 , means starting move starting point and ending point
                startIndex = currentStartIndex;
                endIndex = --currentEndIndex;
                result = find(startIndex,endIndex,currentStartIndex,currentEndIndex,count,word);

            }*//*


        } else {
            //return string from subset
            result = String.valueOf(word).substring(startIndex, endIndex + 1);
        }
        System.out.println(result);
        return result;
    }*/

    // can be optimized further
    public static String find1(int startIndex, int endIndex, int currentStartIndex, int currentEndIndex, int count, char[] word) {
        String palindrome = "";
        String palindromeUsingCenter = "";
        String palindromeWithoutCenter = "";
        int palindromeLength = 0;

        String tempPalindrome = "";


        int i = 0;
        for (i = 0; i < word.length; i++) {
            palindromeUsingCenter = findStringUsingCenterIndex(i, word);
            palindromeWithoutCenter = findStringUsingNonCenterIndex(i, word);

            tempPalindrome = palindromeUsingCenter.length() > palindromeWithoutCenter.length() ? palindromeUsingCenter : palindromeWithoutCenter;

            if (tempPalindrome.length() > palindromeLength) {
                palindrome = tempPalindrome;
                palindromeLength = palindrome.length();
            }
            if (palindromeLength == word.length)
                break;
        }
        return palindrome;
    }

    public static String findStringUsingCenterIndex(int centerIndex, char[] word) {
        int left = centerIndex - 1;
        int right = centerIndex + 1;
        int equalCharCount = 0;
        while (left > -1 && right < word.length) {
            if (word[left] != word[right])
                break;

            equalCharCount++;
            left--;
            right++;
        }
        if ((centerIndex - equalCharCount > -1) &&  (centerIndex + equalCharCount < word.length))
            return String.valueOf(word).substring(centerIndex - equalCharCount, centerIndex + equalCharCount + 1);

        return String.valueOf(word[centerIndex]);
    }

    public static String findStringUsingNonCenterIndex(int currentIndex, char[] word) {
        //i am starting from 0 , so i will treat current index as left
        int left = currentIndex;
        int right = currentIndex + 1;
        int equalCharCount = 0;
        while (left > -1 && right < word.length) {
            if (word[left] != word[right])
                break;

            equalCharCount++;
            left--;
            right++;
        }


        // +1 coz it is biased to left]
        if ((equalCharCount != 0) && (currentIndex - equalCharCount + 1 > -1) && (currentIndex + equalCharCount < word.length))
            return String.valueOf(word).substring(currentIndex - equalCharCount + 1, currentIndex + equalCharCount + 1);

        return String.valueOf(word[currentIndex]);
    }
}