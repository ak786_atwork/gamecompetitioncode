import java.util.Scanner;

public class StringPermutation {

    private static int permutations = 0;

    public static void main(String arg[]) {

        Scanner sc = new Scanner(System.in);
        int testCases = sc.nextInt();
        String word = "";

          sc.nextLine();

        while (testCases > 0) {
            word =  sc.nextLine();
            permute(0, "", word.toCharArray());
            testCases--;
        }
        System.out.println("permutations = " + permutations);
    }

    public static void permute(int index, String originalString, char[] currentStringArray) {


        int i = index;
        if (index == currentStringArray.length) {
            permutations++;
            System.out.println(originalString);

        } else {
            for (i = index; i < currentStringArray.length; i++) {
                permute(index + 1, originalString + currentStringArray[i], swap(index, i, currentStringArray));
                currentStringArray = swap(index, i, currentStringArray);
            }
        }
    }

    public static char[] swap(int firstIndex, int secondIndex, char[] currentStringArray) {
        char temp = currentStringArray[firstIndex];
        currentStringArray[firstIndex] = currentStringArray[secondIndex];
        currentStringArray[secondIndex] = temp;

        return currentStringArray;
    }
}
