import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

//working fine
public class Sort012 {

    public static void main(String arg[]) throws IOException {

//        Scanner sc = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int testCases = Integer.parseInt(br.readLine());//sc.nextInt();
        int size;
        int[] array;
        while (testCases > 0) {
//            size = sc.nextInt();
            size =  Integer.parseInt(br.readLine());
            array = new int[size];
//            array = handleArray(sc, array);
            printArray(sortArray(handleArrayWithBufferedReader(br,array)));
//            printArray(handleArrayWithBufferedReaderAndSort(br,array));

            System.out.println("");
            testCases--;

        }
    }

    private static int[] handleArrayWithBufferedReader(BufferedReader br, int[] array) throws IOException {
        // to read multiple integers line
        String line = br.readLine();
        String[] strs = line.trim().split("\\s+");

        // array elements input
        for (int i = 0; i < array.length; i++)
            array[i] = Integer.parseInt(strs[i]);

        return array;
    }

    // this can be optimized further
    private static int[] sortArray(int[] array) {
        int leftIndexToSwap = -1;
        int rightIndexToSwap = -1;
        int j = array.length - 1;
        boolean zeroFinished = false;
        for (int i = 0; i < array.length; i++) {

            if (array[i] == 1 && !zeroFinished) {
                //find zero from right specify bound i
                leftIndexToSwap = i;
                rightIndexToSwap = findNumberIndexFromRight(0,array,i);
                zeroFinished = rightIndexToSwap == -1;
                array = swap(array,leftIndexToSwap,rightIndexToSwap);
            }

            if (array[i] == 2) {
                if (zeroFinished) {
                    leftIndexToSwap = i;
                    rightIndexToSwap = findNumberIndexFromRight(1,array,i);
                    array = swap(array,leftIndexToSwap,rightIndexToSwap);
                }
                else {
                    leftIndexToSwap = i;
                    rightIndexToSwap = findNumberIndexFromRight(0,array,i);
                    zeroFinished = rightIndexToSwap == -1;
                    if (zeroFinished)
                        --i;
                    array = swap(array,leftIndexToSwap,rightIndexToSwap);
                }

            }











            /*if (array[i] != 0) {
                leftIndexToSwap = i;
                rightIndexToSwap = findNumberIndexFromRight(0,array);
                swap(array, leftIndexToSwap,rightIndexToSwap);
            }*/
           /* if (array[j] != 2) {
                rightIndexToSwap = j;
            }*/
        }
        return array;
    }
    private static int findNumberIndexFromRight(int number, int[] array,int bound) {
        for (int i= array.length - 1; i > bound; i-- ) {
            if (number == array[i])
                return i;
        }
        return -1;
    }

    private static int[] swap(int[] array, int firstIndex, int secondIndex) {
        if (firstIndex == -1 || secondIndex == -1)
            return array;

        int temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
        return array;
    }


    private static int[] handleArray(Scanner sc, int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextInt();
        }
        return array;
    }

    private static void printArray(int[] array) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < array.length; i++) {
            stringBuffer.append(array[i] +" ");
        }
        System.out.print(stringBuffer.toString());
    }

    // well and good but not good approach
    private static int[] handleArrayWithBufferedReaderAndSort(BufferedReader br, int[] array) throws IOException {
        int zeroCount = 0;
        int oneCount = 0;
        int twoCount = 0;

        // to read multiple integers line
        String line = br.readLine();
        String[] strs = line.trim().split("\\s+");

        // array elements input

        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(strs[i]);
            if (array[i] == 0)
                ++zeroCount;
            else if (array[i] == 1)
                ++oneCount;
            else
                ++twoCount;
        }
        return sortArray1(array, zeroCount, oneCount, twoCount);
    }

    private static int[] sortArray1(int[] array,int zeroCount, int oneCount, int twoCount) {

        for (int i = 0; i < array.length; i++) {
            if (zeroCount != 0){
                array[i] = 0;
                --zeroCount;
            }
            else if (oneCount != 0){
                array[i] = 1;
                --oneCount;
            }
            else {
                --twoCount;
                array[i] = 2;
            }


        }
        return array;
    }

}



/*


public class Sort012 {

    public static void main(String arg[]) {

        Scanner sc = new Scanner(System.in);

        int testCases = sc.nextInt();
        int size;
        int[] array;
        while (testCases > 0) {
            size = sc.nextInt();
            array = new int[size];
            printArray(handleArrayAndSort(sc,array));
            System.out.println("");
            testCases--;

        }
    }

    private static int[] sortArray1(int[] array,int zeroCount, int oneCount, int twoCount) {
        int leftIndexToSwap = -1;
        int rightIndexToSwap = -1;
        int j = array.length - 1;
        boolean zeroFinished = false;

        for (int i = 0; i < array.length; i++) {
            if (zeroCount != 0){
                array[i] = 0;
                --zeroCount;
            }
            else if (oneCount != 0){
                array[i] = 1;
                --oneCount;
            }
            else {
                --twoCount;
                array[i] = 2;
            }


        }
        return array;
    }

    private static int[] sortArray(int[] array) {
        int leftIndexToSwap = -1;
        int rightIndexToSwap = -1;
        int j = array.length - 1;
        boolean zeroFinished = false;
        for (int i = 0; i < array.length; i++) {

            if (array[i] == 1 && !zeroFinished) {
                //find zero from right specify bound i
                leftIndexToSwap = i;
                rightIndexToSwap = findNumberIndexFromRight(0,array,i);
                zeroFinished = rightIndexToSwap == -1;
                array = swap(array,leftIndexToSwap,rightIndexToSwap);
            }

            if (array[i] == 2) {
                if (zeroFinished) {
                    leftIndexToSwap = i;
                    rightIndexToSwap = findNumberIndexFromRight(1,array,i);
                    array = swap(array,leftIndexToSwap,rightIndexToSwap);
                }
                else {
                    leftIndexToSwap = i;
                    rightIndexToSwap = findNumberIndexFromRight(0,array,i);
                    zeroFinished = rightIndexToSwap == -1;
                    if (zeroFinished)
                        --i;
                    array = swap(array,leftIndexToSwap,rightIndexToSwap);
                }

            }










            */
/*if (array[i] != 0) {
                leftIndexToSwap = i;
                rightIndexToSwap = findNumberIndexFromRight(0,array);
                swap(array, leftIndexToSwap,rightIndexToSwap);
            }*//*

           */
/* if (array[j] != 2) {
                rightIndexToSwap = j;
            }*//*

        }
        return array;
    }
    private static int findNumberIndexFromRight(int number, int[] array,int bound) {
        for (int i= array.length - 1; i > bound; i-- ) {
            if (number == array[i])
                return i;
        }
        return -1;
    }

    private static int[] swap(int[] array, int firstIndex, int secondIndex) {
        if (firstIndex == -1 || secondIndex == -1)
            return array;

        int temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
        return array;
    }


    private static int[] handleArray(Scanner sc, int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextInt();
        }
        return array;
    }
    private static int[] handleArrayAndSort(Scanner sc, int[] array) {
        int zeroCount = 0;
        int oneCount = 0;
        int twoCount = 0;
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextInt();
            if (array[i] == 0)
                ++zeroCount;
            else if (array[i] == 1)
                ++oneCount;
            else
                ++twoCount;
        }
        return sortArray1(array, zeroCount, oneCount, twoCount);
    }

    private static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] +" ");
        }
    }

}

*/
