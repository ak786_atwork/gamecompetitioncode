package kickstart;

import java.util.Scanner;

public class BuildingPalindromes {

    public static int[] alphabetCount = new int[26];

    public static void main (String arg[]) {

        Scanner sc = new Scanner(System.in);

        int testCases = sc.nextInt();
        String alphabets = "";
        int numberOfAlphabets;
        int numberOfQuestions;
        int i = 0;
        while (i < testCases ) {
            numberOfAlphabets = sc.nextInt();
            numberOfQuestions = sc.nextInt();
            sc.nextLine();  // clearing next line
            alphabets = sc.nextLine();
            System.out.println("Case #"+(i+1)+": "+getPalindromeCount(alphabets, numberOfQuestions,sc));

            i++;

        }
    }

    private static int getPalindromeCount(String alphabets, int numberOfQuestions, Scanner sc) {
        int left;
        int right;
        int count = 0;
        while (numberOfQuestions > 0) {
            left = sc.nextInt();
            right = sc.nextInt();

            // not using 0 based indexing
            count += checkIfPalindromePossible(--left,--right,alphabets);

            numberOfQuestions--;
        }
        return count;
    }

    private static int checkIfPalindromePossible(int left, int right, String alphabets) {
        int count = 0;
        int index = 0;
        alphabetCount =  resetArray(alphabetCount);
        // right bound inclusive
        while (left < right + 1) {
            index = alphabets.charAt(left) - 65;
            ++alphabetCount[index];

            left++;
        }
        int oddCount = 0;
        for (int j = 0;j < 26; j++) {
            if (oddCount > 1)
                return 0;
            oddCount += alphabetCount[j] % 2 != 0 ? 1 : 0;
        }
        return 1;
    }

    public static int[] resetArray(int[] array) {
        for (int i= 0; i < array.length; i++)
            array[i] = 0;
        return array;
    }
}
