import java.util.Scanner;

//working
public class RemoveAdjacentDuplicateKeepOne {

    public static void main (String arg[]) {

        Scanner sc = new Scanner(System.in);

        int testCases = sc.nextInt();
        String temp = "";
        sc.nextLine();
        while (testCases > 0) {
            temp =  sc.nextLine();
            System.out.println(getStringAfterDuplicateRemoval(temp.toCharArray()));
            testCases--;

        }
    }

    public static String getStringAfterDuplicateRemoval(char[] word) {
        char lastChar = '0';
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1 ; i < word.length  ; i++) {
            if (word[i-1] != word[i]) {
                stringBuilder.append(word[i-1]);
                lastChar = word[i-1];
            }

        }
        if (lastChar != word[word.length - 1])
            stringBuilder.append(word[ word.length -1]);

        return stringBuilder.toString();
    }

}
