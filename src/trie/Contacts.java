package trie;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/*not working for all cases*/

public class Contacts {

     static Node root;

    static int[] contacts(String[][] queries) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i=0;i<queries.length;i++){
            if (queries[i][0].equals("add")){
                //insert contact
                insertContact(queries[i][1]);
            } else {
                //find contact
                list.add(findPartialWordCount(queries[i][1]));
            }
        }
        int[] array = new int[list.size()];
        for (int i=0;i<list.size();i++)
            array[i] = list.get(i);
        return array;
    }

    public static void insertContact(String name) {
        if (root == null)
            root = new Node(true);

        Node temp = root;
        for (int i=0;i<name.length();i++){
            if (!temp.hashMap.containsKey(name.charAt(i))){
                insertAllChar(temp,name,i);
                break;
            }
            temp = temp.hashMap.get(name.charAt(i));
        }
    }

    private static void insertAllChar(Node node, String name, int nameIndex) {
        Node pre = new Node(true);
        int i;
        for (i = name.length() -1; i > nameIndex;i--){
            Node newNode = new Node(false);
            newNode.hashMap.put(name.charAt(i),pre);
            pre = newNode;
        }
        node.hashMap.put(name.charAt(i),pre);
    }

    public static int findPartialWordCount(String name){
        Node temp = root;
        Node pre = root;
        for (int i=0;i<name.length();i++){
            if (!temp.hashMap.containsKey(name.charAt(i)))
                return 0;
            temp  = temp.hashMap.get(name.charAt(i));
        }

        return findAllPossibleWords(temp);
    }

    private static int findAllPossibleWords(Node temp) {
        HashMap<Character,Node> hashMap = temp.hashMap;
        int count = 0;
        if (temp.endOfWord)
            ++count;
        for (Map.Entry<Character, Node> entry : temp.hashMap.entrySet()){
            count += findAllPossibleWords(entry.getValue());
        }
        return count;
    }


    static class Node {
        public HashMap<Character,Node> hashMap;
        public boolean endOfWord;
        public int wordStartingWithPrefix = 0;

        public Node(boolean endOfWord) {
            this.hashMap = new HashMap<>();
            this.endOfWord = endOfWord;
        }


        public void setEndOfWord(boolean endOfWord) {
            this.endOfWord = endOfWord;
        }

    }
}
