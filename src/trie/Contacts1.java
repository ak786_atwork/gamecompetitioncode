package trie;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/*successful submission hackerrank*/


public class Contacts1 {

    static Node root;

    static int[] contacts(String[][] queries) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i=0;i<queries.length;i++){
            if (queries[i][0].equals("add")){
                //insert contact
                insertContact(queries[i][1]);
            } else {
                //find contact
                list.add(findPartialWordCount(queries[i][1]));
            }
        }
        int[] array = new int[list.size()];
        for (int i=0;i<list.size();i++)
            array[i] = list.get(i);
        return array;
    }

    public static void insertContact(String name) {
        if (root == null)
            root = new Node(true);

        Node temp = root;
        for (int i=0;i<name.length();i++){
            if (!temp.hashMap.containsKey(name.charAt(i))){
                insertAllChar(temp,name,i);
                return;
            }
            temp.wordStartingWithPrefix++;
            temp = temp.hashMap.get(name.charAt(i));
        }
        //word is already present as a prefix of another word, so mark as end of word and increment count
        temp.wordStartingWithPrefix++;
        temp.endOfWord = true;
    }

    private static void insertAllChar(Node node, String name, int nameIndex) {
        Node pre = new Node(true);
        pre.wordStartingWithPrefix++;
        int i;
        for (i = name.length() -1; i > nameIndex;i--){
            Node newNode = new Node(false);
            newNode.hashMap.put(name.charAt(i),pre);
            newNode.wordStartingWithPrefix++;
            pre = newNode;
        }
        node.hashMap.put(name.charAt(i),pre);
        node.wordStartingWithPrefix++;
    }

    public static int findPartialWordCount(String name){
        if(root == null)
            return 0;
        Node temp = root;
        Node pre = root;
        for (int i=0;i<name.length();i++){
            if (!temp.hashMap.containsKey(name.charAt(i)))
                return 0;
            temp  = temp.hashMap.get(name.charAt(i));
        }

        return temp.wordStartingWithPrefix;
    }


    static class Node {
        public HashMap<Character,Node> hashMap;
        public boolean endOfWord;
        public int wordStartingWithPrefix = 0;

        public Node(boolean endOfWord) {
            this.hashMap = new HashMap<>();
            this.endOfWord = endOfWord;
        }


        public void setEndOfWord(boolean endOfWord) {
            this.endOfWord = endOfWord;
        }

    }
}
