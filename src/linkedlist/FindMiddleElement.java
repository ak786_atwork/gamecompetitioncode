package linkedlist;

/*submitted on geeksforgeeks working*/

public class FindMiddleElement {
    class Node {
        int data;
        Node next;

        Node(int d) {
            data = d;
            next = null;
        }
    }

    // Function to find middle element a linked list

    int getMiddle(Node head) {
        // Your code here.
        Node slowPtr = head;
        Node fastPtr = head;

        while (fastPtr != null && fastPtr.next != null) {
            slowPtr = slowPtr.next;
            fastPtr = fastPtr.next.next;
        }

        return slowPtr.data;

    }
}
