package linkedlist;

/*submitted on geeksforgeeks*/

public class ReverseLinkedList {

    class Node {
        int data;
        Node next;

        Node(int d) {
            data = d;
            next = null;
        }
    }

    // This function should reverse linked list and return
    // head of the modified linked list.
    Node reverseList(Node head)
    {
        // add code here
        Node pre = null;
        Node next = null;
        Node current= head;
        while (current != null){
            next = current.next;
            current.next = pre;
            pre = current;
            current = next;
        }
        return pre;
    }
}
