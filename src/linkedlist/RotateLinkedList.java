package linkedlist;

/*submitted on geeksforgeeks*/

public class RotateLinkedList {
    /*  This function should rotate list counter-
  clockwise by k and return new head (if changed) */
    public Node rotate(Node head, int k) {
        // add code here.
        if (k == 0 || head.next == null)
            return head;

        Node temp = head;
        int length = 0;
        while (++length < k && temp != null) {
            temp = temp.next;
        }

        if (temp == null)
            --length;

        //check if k is larger than length
        if (k > length) {
            k = k % length;
            length = 0;

            if (k == 0)
                return head;

            temp = head;
            while (++length < k && temp != null) {
                temp = temp.next;
            }
        }

        //check if length and rotation are equal
        if (k == 0 && temp.next == null)
            return head;

        Node newEndNode = temp;
        while (temp != null && temp.next != null) {
            temp = temp.next;
        }

        //join lists
        temp.next = head;
        head = newEndNode.next;
        newEndNode.next = null;

        return head;


    }
}
