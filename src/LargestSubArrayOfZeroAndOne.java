import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LargestSubArrayOfZeroAndOne {

    public static void main(String arg[]) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

//        int testCases = 1;//Integer.parseInt(br.readLine());//sc.nextInt();
        int testCases = Integer.parseInt(br.readLine());//sc.nextInt();
        int size;
        int[] array;
        while (testCases > 0) {
            size = Integer.parseInt(br.readLine());
            array = new int[size];
            array = handleArrayWithBufferedReader(br, array);
            System.out.println(findSubarrayLength(array));
            testCases--;

        }
    }
    private static int findSubarrayLength(int[] array) {

        int zeroCount = 0;
        int oneCount = 0;
        int zeroStartIndex = 0;
        int oneStartIndex = 0;
        int zeroEndIndex = 0;
        int oneEndIndex = 0;

        boolean zeroStartIndexSet = false;
        boolean oneStartIndexSet = false;


        for (int i = 0; i < array.length; i++) {
            if (!zeroStartIndexSet || !oneStartIndexSet) {
                if (array[i] == 0) {
                    zeroStartIndex = i;
                    zeroEndIndex = i;
                    zeroStartIndexSet = true;
                } else {
                    oneEndIndex = i;
                    oneStartIndex = i;
                    oneStartIndexSet = true;
                }
            }
            if (array[i] == 0 ) {
                ++zeroCount;
                zeroEndIndex = i;
            } else {
                ++oneCount;
                oneEndIndex = i;
            }

        }

        System.out.println(zeroCount + " " +zeroStartIndex +" "+ zeroEndIndex);
        System.out.println(oneCount + " " +oneStartIndex +" "+ oneEndIndex);

        if (zeroCount == oneCount)
            return array.length;
        else if (zeroCount > oneCount) {
            //first min and then max
            return getSolution(oneCount, oneStartIndex, oneEndIndex, zeroCount, zeroStartIndex, zeroEndIndex);
        } else {
            return getSolution(zeroCount,zeroStartIndex, zeroEndIndex, oneCount, oneStartIndex,oneEndIndex);
        }


    }

    private static int getSolution(int minCount, int minStartIndex, int minEndIndex, int maxCount, int maxStartIndex, int maxEndIndex) {
        if (minEndIndex - minStartIndex + 1 >= 2 * minCount )
            return minEndIndex - minStartIndex + 1;
        else
            return 2 * minCount;
    }

    private static int[] handleArrayWithBufferedReader(BufferedReader br, int[] array) throws IOException {
        // to read multiple integers line
        String line = br.readLine();
        String[] strs = line.trim().split("\\s+");

        // array elements input
        for (int i = 0; i < array.length; i++)
            array[i] = Integer.parseInt(strs[i]);

//        int array1[] = {1, 3 ,5, 2, 2};
        return array;
    }

}
