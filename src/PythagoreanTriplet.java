import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
//not working
/* Pythagoras and the Babylonians gave a formula for generating (not necessarily primitive) triples as
(2m,m^2-1,m^2+1), */
public class PythagoreanTriplet {

    public static void main(String arg[]) throws IOException {

//        Scanner sc = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

//        int testCases = 1;//Integer.parseInt(br.readLine());//sc.nextInt();
        int testCases = Integer.parseInt(br.readLine());//sc.nextInt();
        int size;
        int[] array;
        while (testCases > 0) {
//            size = sc.nextInt();
            size = Integer.parseInt(br.readLine());
            array = new int[size];
            array = handleArrayWithBufferedReader(br, array);
            System.out.println(tripletExist(array));
            testCases--;

        }
    }

    private static String tripletExist(int[] array) {
        HashMap<Integer, Integer> hashMap = prepareValueCount(array);
        int m = 0;
        int side = 0;
        int hypotenuse = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                m = array[i] / 2;

                hypotenuse = m * m + 1;
                side = m * m - 1;
                if ((hashMap.containsKey(hypotenuse) && hashMap.containsKey(side))) {
                    if (m == 1) {
                        if (hashMap.get(array[i]) > 1)
                            return "Yes";
                    } else {
                        return "Yes";
                    }

                }

            }
        }
        return "No";
    }

    public static HashMap<Integer, Integer> prepareValueCount(int[] array) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            if (hashMap.containsKey(array[i])) {
                hashMap.put(array[i], hashMap.get(array[i]) + 1);
            } else {
                hashMap.put(array[i], 1);
            }
        }
        return hashMap;
    }

    private static int[] handleArrayWithBufferedReader(BufferedReader br, int[] array) throws IOException {
        // to read multiple integers line
        String line = br.readLine();
        String[] strs = line.trim().split("\\s+");

        // array elements input
        for (int i = 0; i < array.length; i++)
            array[i] = Integer.parseInt(strs[i]);

//        int array1[] = {1, 3 ,5, 2, 2};
        return array;
    }

}

//74 87 22 46 25 73 71 30 78 74 98 13 87 91 62 37 56 68 56 75 32 53 51 51 42 25 67 31 8 92 8 38 58 88 54 84 46 10 10 59 22 89 23 47 7 31 14 69 1 92 63 56 11 60 25
