import java.util.Scanner;
import java.util.Stack;

//working
public class RemoveAdjacentDuplicate {

    static Stack<Character> stack = new Stack<>();
    public static void main (String arg[]) {

        Scanner sc = new Scanner(System.in);

        int testCases = sc.nextInt();
        String temp = "";
        sc.nextLine();
        while (testCases > 0) {
            temp =  sc.nextLine();
            System.out.println(getStringAfterDuplicateRemoval2(temp.toCharArray()));
            testCases--;

        }
    }

    //this is also working
    public static String getStringAfterDuplicateRemoval(char[] word) {
        char lastChar = '0';
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1 ; i < word.length  ; i++) {
            if (word[i-1] != word[i] && lastChar != word[i-1]) {
                stringBuilder.append(word[i-1]);
            }
            lastChar = word[i-1];
        }
        if (lastChar != word[word.length - 1])
            stringBuilder.append(word[ word.length -1]);

        return stringBuilder.toString();
    }

    // not working
    public static String getStringAfterDuplicateRemoval1(char[] word) {
        char lastChar = '0';
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1 ; i < word.length  ; i++) {
            if (word[i-1] == word[i]) {
                i = getLastRepeatIndex(word, i-1);
            }
            else
                stringBuilder.append(word[i-1]);
        }

        return stringBuilder.toString();
    }

    public static int getLastRepeatIndex(char[] word, int currentIndex) {
        int i = currentIndex + 1;
        while (i < word.length) {
            if (word[i-1] != word[i])
                break;
            i++;
        }
        return i;
    }

    public static String getStringAfterDuplicateRemoval2(char[] word) {
        char lastChar = '0';
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 1 ; i < word.length  ; i++) {
            if (word[i-1] == word[i]) {
                i = getLastRepeatIndex(word, i-1);
            }
            else{
//                stack.push(word[i-1]);
                updateStackState(i-1,word);
            }
//                stringBuilder.append(word[i-1]);
        }
        updateStackState(word.length -1 , word);
        return getStringFromStack();
    }

    public static void updateStackState(int currentIndex, char[] word) {
        if (!stack.isEmpty() && stack.peek() == word[currentIndex])
                stack.pop();
            else {
                if (currentIndex < 1 || word[currentIndex - 1] != word[currentIndex])
                    stack.push(word[currentIndex]);
        }

    }

    public static String getStringFromStack() {
        StringBuilder stringBuilder = new StringBuilder();
        while (!stack.isEmpty()) {
            stringBuilder.append(stack.pop());
        }
        return stringBuilder.reverse().toString();
    }
}
