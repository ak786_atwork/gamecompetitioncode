import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


//working fine
public class MissingNaturalNumber {

    public static void main(String arg[]) throws IOException {

//        Scanner sc = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int testCases = Integer.parseInt(br.readLine());//sc.nextInt();
        int size;
        int[] array;
        while (testCases > 0) {
            size =  Integer.parseInt(br.readLine());
            array = new int[size-1];
            array =  handleArrayWithBufferedReader(br,array);
            System.out.println(findMissingNumber(array,size));
            testCases--;

        }
    }

    private static int findMissingNumber(int[] array, int size) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        int missingNumber = size * (size + 1)/2;
        return missingNumber - sum;
    }


    private static int[] handleArrayWithBufferedReader(BufferedReader br, int[] array) throws IOException {
        // to read multiple integers line
        String line = br.readLine();
        String[] strs = line.trim().split("\\s+");

        // array elements input
        for (int i = 0; i < array.length; i++)
            array[i] = Integer.parseInt(strs[i]);

        return array;
    }

}
