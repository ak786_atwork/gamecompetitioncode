import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SubarrayWIthMaxSumKadaneAlgo {

    public static void main(String arg[]) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

//        int testCases = 1;//Integer.parseInt(br.readLine());//sc.nextInt();
        int testCases = Integer.parseInt(br.readLine());//sc.nextInt();
        int size;
        int[] array;
        while (testCases > 0) {
            size = Integer.parseInt(br.readLine());
            array = new int[size];
            array = handleArrayWithBufferedReader(br, array);
            System.out.println(getMaximumSumInSubarray(array));
            testCases--;

        }
    }

    private static int getMaximumSumInSubarray(int[] array) {
        int totalArraySum = getArraySum(array);
        int maxSumInSubArray = 0;
        int tempLeftSum = 0;
        int tempRightSum = 0;

        int maxSum = array[0];
        int net = 0;
        int j = array.length - 1;
        for (int i = 0; i < array.length; i++) {
            if (maxSum < net + array[i]) {
                maxSum = net + array[i];
            }
            net += array[i];
            net = net < 0 ? 0 : net;
        }
        return maxSum;
    }

    private static int[] handleArrayWithBufferedReader(BufferedReader br, int[] array) throws IOException {
        // to read multiple integers line
        String line = br.readLine();
        String[] strs = line.trim().split("\\s+");

        // array elements input
        for (int i = 0; i < array.length; i++)
            array[i] = Integer.parseInt(strs[i]);

//        int array1[] = {1, 3 ,5, 2, 2};
        return array;
    }

    public static int getArraySum(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++)
            sum += array[i];

        return sum;
    }



}
