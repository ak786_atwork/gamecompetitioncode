package tree;

import java.util.LinkedList;
import java.util.Stack;

public class TopViewTree {

    // A Binary Tree node
    class Node
    {
        int data;
        Node left, right;
        Node(int item)
        {
            data = item;
            left = right = null;
        }
    }

    private static final char L = 'L';
    private static final char R = 'R';


    static void topView(Node root) {
        if (root == null)
            return;

        int maxLeft = 0;
        int maxRight = 0;

        LinkedList<Integer> linkedList = new LinkedList<>();

        linkedList.add(root.data);
        Stack<CallStack> rightStack = new Stack<>();

        boolean leftSubtree = true;
        if (root.left != null){
            traverse(root.left,linkedList, rightStack, L, 0,0,leftSubtree,maxLeft,maxRight);
        }
        leftSubtree = false;
        if (root.right != null){
            traverse(root.right,linkedList,rightStack, R, 0,0,leftSubtree,maxLeft,maxRight);
        }

        CallStack callStack = null;
        while (!rightStack.isEmpty()){
            callStack = rightStack.pop();
            traverse(callStack.node,linkedList,rightStack,callStack.subtree,callStack.currentLeft,callStack.currentRight,leftSubtree,maxLeft,maxRight);
        }

        printLinkedList(linkedList);
    }

    private static void printLinkedList(LinkedList<Integer> linkedList) {
        for (Integer i : linkedList)
            System.out.print(i+" ");
    }

    static class CallStack{
        Node node;
        int currentLeft;
        int currentRight;
        char subtree;

        public CallStack(Node node, int currentLeft, int currentRight, char subtree) {
            this.node = node;
            this.currentLeft = currentLeft;
            this.currentRight = currentRight;
            this.subtree = subtree;
        }
    }

    private static void traverse(Node node, LinkedList<Integer> linkedList,Stack<CallStack> rightStack, char c, int currentLeft, int currentRight,boolean leftSubtree,int maxLeft,int maxRight) {
        if (node == null)
            return;

        if (c == L){
            ++currentLeft;
            --currentRight;
            if (currentLeft > maxLeft){
                maxLeft = currentLeft;
                linkedList.addFirst(node.data);
            }
            // check left right node null, and update currentLeft, currentRight and traverse
            if (node.left != null)
                traverse(node.left,linkedList,rightStack, c,currentLeft,currentRight,leftSubtree,maxLeft,maxRight);
            if (node.right != null){
                if (leftSubtree){
                    //add right node to call stack
                    rightStack.push(new CallStack(node.right,currentLeft,currentRight,c));
                } else {
                    //so we are traversing right subtree
                    traverse(node.right,linkedList,rightStack,c,currentLeft,currentRight,leftSubtree,maxLeft,maxRight);
                }
            }
        } else {
            //parent node is right sub tree
            --currentLeft;
            ++currentRight;

            if (currentRight > maxRight){
                maxRight = currentRight;
                linkedList.addLast(node.data);
            }

            // check left right node null, and update currentLeft, currentRight and traverse
            if (node.left != null)
                traverse(node.left,linkedList,rightStack, c,currentLeft,currentRight,leftSubtree,maxLeft,maxRight);
            if (node.right != null){
                if (leftSubtree){
                    //add right node to call stack
                    rightStack.push(new CallStack(node.right,currentLeft,currentRight,c));
                } else {
                    //so we are traversing right subtree
                    traverse(node.right,linkedList,rightStack,c,currentLeft,currentRight,leftSubtree,maxLeft,maxRight);
                }
            }
        }

    }
}
