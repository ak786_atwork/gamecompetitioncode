package tree;

import java.util.HashMap;
import java.util.LinkedList;

/*submitted on geeksforgeeks*/

public class TopView1 {

    public static void main(String[] args) {
        Node root = new Node(1);
        Node node1 = new Node(2);
        Node node2 = new Node(3);
        Node node4 = new Node(4);
        Node node5 = new Node(5);

        root.left = node1;
        root.right = node2;

        node1.right = node4;
        node2.left = node5;

        topView(root);
    }

    // A Binary Tree node
    static class Node {
        int data;
        Node left, right;

        Node(int item) {
            data = item;
            left = right = null;
        }
    }

    static class MetaData {
        int currentLeft;
        int currentRight;
        char subtree;

        public MetaData(int currentLeft, int currentRight, char subtree) {
            this.currentLeft = currentLeft;
            this.currentRight = currentRight;
            this.subtree = subtree;
        }

    }

    static int maxLeft = -1;
    static int maxRight = -1;

    static void topView(Node root) {
        if (root == null)
            return;

        maxLeft = -1;
        maxRight = -1;

        LinkedList<Node> queue = new LinkedList<>();
        LinkedList<Node> outputQueue = new LinkedList<>();
        queue.add(root);

        HashMap<Node, MetaData> hashMap = new HashMap<>();
        hashMap.put(root, new MetaData(0, 0, 'p'));

        int popCount = 1;


        Node temp = null;
        MetaData metaData = null;
        boolean startNode = true;

        while (!queue.isEmpty()) {

            if (startNode) {
                startNode = false;
                addExtremeNodes(queue, outputQueue, hashMap);
            }


            temp = queue.poll();

            metaData = hashMap.get(temp);
            popCount--;


            if (metaData != null && temp.left != null) {
           /*     if (metaData.subtree == 'r') {
                    hashMap.put(temp.left, new MetaData(metaData.currentLeft + 1, metaData.currentRight - 1, 'l'));
                } else {        //parent or left node
                    hashMap.put(temp.left, new MetaData(metaData.currentLeft + 1, metaData.currentRight - 1, 'l'));
                }*/

                hashMap.put(temp.left, new MetaData(metaData.currentLeft + 1, metaData.currentRight - 1, 'l'));
                queue.addLast(temp.left);
            }

            if (metaData != null && temp.right != null) {
              /*  if (metaData.subtree == 'l') {
                    hashMap.put(temp.right, new MetaData(metaData.currentLeft - 1, metaData.currentRight + 1, 'r'));
                } else {        //parent or left node
                    hashMap.put(temp.right, new MetaData(metaData.currentLeft - 1, metaData.currentRight + 1, 'r'));
                }*/

                hashMap.put(temp.right, new MetaData(metaData.currentLeft - 1, metaData.currentRight + 1, 'r'));
                queue.addLast(temp.right);
            }



            if (popCount == 0) {
                //print node
                popCount = queue.size();
                startNode = true;
            }


        }


        printNodes(outputQueue);

    }

    private static void printNodes(LinkedList<Node> outputQueue) {
//        System.out.println();
        for (Node node : outputQueue) {
            System.out.print(node.data + " ");
        }
//        System.out.println();
    }

    private static void addExtremeNodes(LinkedList<Node> queue, LinkedList<Node> outputQueue, HashMap<Node, MetaData> hashMap) {

//        System.out.println("size = "+queue.size());
//        System.out.println("maxleft = "+maxLeft+" maxright = "+maxRight);
        // printNodes(queue);

        MetaData tempMetaData = null;
        Node temp = null;
        Node maxLeftNode = null;
        Node maxRightNode = null;

        int tempLeft = -1;
        int tempRight = -1;

        for (Node node : queue) {
            if (hashMap.containsKey(node)) {
                tempMetaData = hashMap.get(node);
                if (tempMetaData.currentLeft > maxLeft) {
                    maxLeft = tempMetaData.currentLeft;
                    maxLeftNode = node;
                    continue;
                }
                if (tempMetaData.currentRight > maxRight) {
                    maxRight = tempMetaData.currentRight;
                    maxRightNode = node;
                }

            }
        }
        if (maxLeftNode == maxRightNode && maxLeftNode != null) {
            // root node
            outputQueue.add(maxLeftNode);
        } else {
            if (maxLeftNode != null) {
                //extreme left found
                outputQueue.addFirst(maxLeftNode);
            }
            if (maxRightNode != null) {
                outputQueue.addLast(maxRightNode);
            }
        }
    }
}
