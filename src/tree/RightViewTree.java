package tree;

//working submitted to geeksforgeeks

import java.util.LinkedList;

public class RightViewTree {
    int maxLevel;
    void rightView(Node root)
    {
        // Your code here
        if (root == null)
            return;
        printRightView(root,0);
        // printRightViewIteration(root);
    }

    void printRightView(Node node, int currentLevel){
        if (node == null)
            return;
        ++currentLevel;
        if (currentLevel > maxLevel) {
            maxLevel = currentLevel;
            System.out.print( node.data+" ");
        }

        if (node.right != null)
            printRightView(node.right,currentLevel);

        if (node.left != null)
            printRightView(node.left,currentLevel);
    }

    void printRightViewIteration(Node root) {
        if (root == null)
            return;
        LinkedList<Node> queue = new LinkedList<>();
        queue.addLast(root);
        int popCount = 1;

        Node temp = null;
        boolean startNode = true;

        while (!queue.isEmpty()){
            temp = queue.poll();
            popCount--;

            if (startNode){
                System.out.print(temp.data + " ");
                startNode = false;
            }

            if (temp.right != null)
                queue.addLast(temp.right);

            if (temp.left !=null)
                queue.addLast(temp.left);

            if (popCount == 0){
                //print node
                popCount = queue.size();
                startNode = true;
            }


        }
    }

    // A Binary Tree node
    class Node
    {
        int data;
        Node left, right;
        Node(int item)
        {
            data = item;
            left = right = null;
        }
    }

}

