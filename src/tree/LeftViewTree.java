package tree;

//submitted geeksforgeeks working

import java.util.LinkedList;

public class LeftViewTree {
    int maxLevel;
    void leftView(Node root)
    {
        // Your code here
        if (root == null)
            return;
        printLeftView(root,0);
        // printLeftViewIteration(root);
    }

    void printLeftView(Node node, int currentLevel){
        if (node == null)
            return;
        ++currentLevel;
        if (currentLevel > maxLevel) {
            maxLevel = currentLevel;
            System.out.print( node.data+" ");
        }
        if (node.left != null)
            printLeftView(node.left,currentLevel);

        if (node.right != null)
            printLeftView(node.right,currentLevel);
    }

    void printLeftViewIteration(Node root) {
        if (root == null)
            return;
        LinkedList<Node> queue = new LinkedList<>();
        queue.addLast(root);
        int popCount = 1;

        Node temp = null;
        boolean startNode = true;

        while (!queue.isEmpty()){
            temp = queue.poll();
            popCount--;

            if (startNode){
                System.out.print(temp.data + " ");
                startNode = false;
            }

            if (temp.left !=null)
                queue.addLast(temp.left);

            if (temp.right != null)
                queue.addLast(temp.right);

            if (popCount == 0){
                //print node
                popCount = queue.size();
                startNode = true;
            }


        }
    }

    // A Binary Tree node
    class Node
    {
        int data;
        Node left, right;
        Node(int item)
        {
            data = item;
            left = right = null;
        }
    }

}

