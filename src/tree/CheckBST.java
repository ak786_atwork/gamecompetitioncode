package tree;

/*submitted on geeksforgeeks working fine*/


import java.util.ArrayList;

public class CheckBST {
    // A Binary Tree node
    class Node
    {
        int data;
        Node left, right;
        Node(int item)
        {
            data = item;
            left = right = null;
        }
    }
    int isBST(Node root)
    {
        if (root == null || (root.left == null && root.right == null))
            return 1;
        // Your code here
        ArrayList<Integer> arrayList = new ArrayList<>();
        inOrderTraversal(root,arrayList);

        int temp = arrayList.get(0);
        for (int i= 1;i<arrayList.size();i++){
            if (arrayList.get(i) < temp)
                return 0;
            temp = arrayList.get(i);
        }
        return 1;
    }

    void inOrderTraversal(Node node, ArrayList<Integer> arrayList) {
        if (node == null)
            return;

        if (node.left != null){
            inOrderTraversal(node.left,arrayList);
        }
        arrayList.add(node.data);
        if (node.right != null)
            inOrderTraversal(node.right,arrayList);
    }
}
