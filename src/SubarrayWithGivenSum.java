import java.util.Scanner;
//working but optimize it
public class SubarrayWithGivenSum {

    public static void main(String arg[]) {

        Scanner sc = new Scanner(System.in);

        int testCases = sc.nextInt();
        int size;
        int sum;
        int[] array;
        int[] resultIndex = new int[2];
        while (testCases > 0) {
            size = sc.nextInt();
            sum = sc.nextInt();
            array = new int[size];
            array = handleArray(sc, array);
            printIndexsOfSubarray(array, sum);
            testCases--;

        }
    }

    private static void printIndexsOfSubarray(int[] array, int sum) {
        int startIndex = 0;
        int endIndex = -1;
        int sumNotExist = -1;
        int temp = sum;

        for (int i = 0; i < array.length; i++) {
            temp -= array[i];

            if (temp == 0) {
                endIndex = i;
                break;
            } else if (temp < 0) {

                //temp < 0  // update  start index
//                temp += array[startIndex] ;
                temp = sum;
                startIndex += 1;
                // as it will increment
                i = startIndex - 1;
            }
        }

        if (endIndex == -1)
            System.out.println("-1");
        else
            System.out.println(++startIndex + " " + (++endIndex));

    }

    private static int[] handleArray(Scanner sc, int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = sc.nextInt();
        }
        return array;
    }

}
